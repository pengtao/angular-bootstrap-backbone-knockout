/*var app = angular.module('myApp', ['ngGrid']);
app.controller('MyCtrl', function($scope) {
    $scope.myData = [{name: "Moroni", age: 50,price: 50,qty:1},
                     {name: "Tiancum", age: 43,price: 50,qty:1},
                     {name: "Jacob", age: 27,price: 50,qty:1},
                     {name: "Nephi", age: 29,price: 50,qty:1},
                     {name: "Enos", age: 34,price: 50,qty:1}];
	$scope.total = 0;
	if($scope.myData!=null && $scope.myData.length>0){
		for(var i = 0;i<$scope.myData.length;i++){
			 $scope.total+=$scope.myData[i].price*$scope.myData[i].qty;
		}
	}
	$scope.$watch('total',function(){
		//alert($scope.total);
	});
	$scope.$watch('myData',function(){
		
		$scope.total = 0;
		for(var i = 0;i<$scope.myData.length;i++){
			 $scope.total+=$scope.myData[i].price*$scope.myData[i].qty;
		}
	},true);
});
*/
var app = angular.module('myApp', ['ngGrid']);
app.controller('MyCtrl', function($scope) {
    $scope.mySelections = [];
    $scope.myData = [{name: "Moroni", age: 50, aa: 50},
                     {name: "Tiancum", age: 43, aa: 50},
                     {name: "Jacob", age: 27, aa: 50},
                     {name: "Nephi", age: 29, aa: 50},
                     {name: "Enos", age: 34, aa: 50}];
	$scope.$watch('myData',function(){
		$scope.myDataTotal = [];
		$scope.myDataCenterTotal = [];
		var total = 0;
		
		for(var i = 0;i<$scope.myData.length;i++){
			$scope.myDataTotal.push({doubleAge:$scope.myData[i].age*1+$scope.myData[i].aa*1});
			total += $scope.myData[i].age*1;
		}
		$scope.myDataCenterTotal.push({total:total});
	},true);
	
    /*$scope.gridOptions = { 
      data: 'myDataTotal',
      selectedItems: $scope.mySelections,
      multiSelect: false
    };
	,
        columnDefs: [{field: 'name', displayName: 'Name', enableCellEdit: true}, 
                     {field:'age', displayName:'Age', enableCellEdit: true}]
	*/
	$scope.gridOptions = { 
        data: 'myData',
        enableCellSelection: true,
        enableRowSelection: false,
        enableCellEditOnFocus: true,
		columnDefs: [{field: 'name', displayName: 'Name', enableCellEdit: true}, 
                     {field:'age', displayName:'Age', enableCellEdit: true},
					 {field:'aa', displayName:'Aa', enableCellEdit: true},
					 {field:'age*1+row.entity.aa*1', displayName:'DoubleAge', enableCellEdit: true}]
    };
	$scope.gridOptionsTotal = {
		data: 'myDataTotal',
        enableCellSelection: false,
        enableRowSelection: false,
        enableCellEditOnFocus: false
	};
	$scope.gridCenterTotal = {
		data: 'myDataCenterTotal',
        enableCellSelection: false,
        enableRowSelection: false,
        enableCellEditOnFocus: false
	};
});