写法1：
var myViewModel = {
	personName : ko.observable('Bob'),
	personAge : ko.observable(123),
	
	numberOfClicks : ko.observable(0),
	//创建方法
	incrementClickCounter : function () {
		var previousCount =this.numberOfClicks();
		this.numberOfClicks(Number(previousCount) +1);
	},
	
	//数组
	people :[
		{firstName: 'Bert', lastName: 'Bertington'},
		{firstName: 'Charles', lastName: 'Charlesforth'},
		{firstName: 'Denise', lastName: 'Dentiste'}
	]
};
//设置属性
myViewModel.personName("aaa");
myViewModel.personName("aaa").personAge(123);
显示订阅：
var subscription =  myViewModel.personName.subscribe(function(newValue) {
	alert("The person's new name is " + newValue);
});
//取消订阅
subscription.dispose();

//直接调用方法
viewModel.incrementClickCounter();



ko.applyBindings(myViewModel);//,document.getElementById("click")