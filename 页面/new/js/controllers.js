myApp.controller("HelloController",function($scope,$http){
	$http.get('java.json').success(function(data, status, headers, config) {
		//返回回来的data.table是一个有序的数组，并且是降序
		$scope.data = data;
		$scope.flag = false;
		$scope.template = "<input type='text' >";
		$scope.isDescOrAsc = "sortDesc";
		$scope.sort = function(){
			var dataObj = data.table;
			if($scope.flag){
				$scope.isDescOrAsc = "sortDesc";
				$scope.flag = false;
				dataObj.sort(function(a,b){
					return b[1]-a[1];
				});
			}else{
				$scope.flag = true;
				$scope.isDescOrAsc = "sortAsc";
				dataObj.sort(function(a,b){
					return a[1]-b[1];
				});
			}
			
		};
	});
	
});